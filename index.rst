.. daft documentation master file, created by
   sphinx-quickstart on Tue Nov 26 17:11:52 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Elita's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 2

   intro.rst
   gitdeploy.rst
   endpoints.rst
   urlmap.rst


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

